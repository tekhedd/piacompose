#!/bin/bash
#
# Enable port forwarding -- tek's docker version
#
# Modified to present port as a raw output string, and store
# the random ID within the container so it only changes when
# the container is restarted.
#
# Requirements:
#   your Private Internet Access user and password as arguments
#
# Usage:
#  ./port_forward.sh <user> <password>

client_id_file=/var/lib/transmission-daemon/client-id

error( )
{
  echo "$@" 1>&2
  exit 1
}

error_and_usage( )
{
  echo "$@" 1>&2
  usage_and_exit 1
}

usage( )
{
  echo "Usage: `dirname $0`/$PROGRAM <user> <password>" 1>&2
}

usage_and_exit( )
{
  usage
  exit $1
}

version( )
{
  echo "$PROGRAM version $VERSION" 1>&2
}


port_forward_assignment( )
{
  if [ "$(uname)" == "Linux" ]; then
    local_ip=`ip addr show tun0 | grep inet | cut -d' ' -f6 | cut -d/ -f1`
    # For docker: save client_id and reuse for the life of one docker session
    if [ -f $client_id_file ] ; then
      echo "Fetching client id from $client_id_file" 1>&2
      client_id=`cat $client_id_file`
    else
      echo "Creating new client_id" 1>&2
      client_id=`head -n 100 /dev/urandom | sha256sum | tr -d " -"`
      echo "${client_id}" > ${client_id_file}
    fi
  elif [ "$(uname)" == "Darwin" ]; then
    local_ip=`ifconfig tun0 | grep "inet " | cut -d\  -f2|tee /tmp/vpn_ip`
    client_id=`head -n 100 /dev/urandom | shasum -a 256 | tr -d " -"`
  else
    error "System type not found"
    exit 1
  fi

  json=`curl "http://209.222.18.222:2000/?client_id=$client_id" 2>/dev/null`
  if [ "$json" == "" ]; then
    json='Port forwarding is already activated on this connection, has expired, or you are not connected to a PIA region that supports port forwarding'
    port=0
  else
    port=`echo "${json}" | jq -r .port`
  fi

  # port=`wget -q --post-data="user=$USER&pass=$PASSWORD&client_id=$client_id&local_ip=$local_ip" -O - 'https://www.privateinternetaccess.com/vpninfo/port_forward_assignment' | jq -r .port`
  echo $port
}

EXITCODE=0
PROGRAM=`basename $0`
VERSION='2.1-transmission'
USER=$1
PASSWORD=$2

while test $# -lt 2
do
  case $1 in
  --usage | --help | -h )
    usage_and_exit 0
    ;;
  --version | -v )
    version
    exit 0
    ;;
  *)
    error_and_usage "Unrecognized option: $1"
    ;;
  esac
  shift
done

port_forward_assignment

exit 0
