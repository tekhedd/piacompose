#!/bin/sh

# -- Proxy port test --
# Find the proxy port that was configured at startup
CONFPORT=`jq '."peer-port"' -r < /var/lib/transmission-daemon/info/settings.json `

# Find the port that is currently configured
PEER_PORT=`/usr/local/bin/port_forward.sh ${VPNUSER} ${VPNPASS}`

if [ "$CONFPORT" != "$PEER_PORT" ] ; then
    echo "Proxy port mismatch"
    exit 1
fi

