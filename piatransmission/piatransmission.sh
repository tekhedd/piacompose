#!/bin/bash

TR_PEER_PORT=`/usr/local/bin/port_forward.sh ${VPNUSER} ${VPNPASS}`
echo "TR_PEER_PORT=${TR_PEER_PORT}"

# Can't get port? Maybe the VPN isn't up yet? Don't start.
[ $? ] || exit 1
[ -z "${TR_PEER_PORT}" ] && exit 1

export TR_PEER_PORT


exec /sbin/tini -- /usr/bin/transmission.sh "$@"
